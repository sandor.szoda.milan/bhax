#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    WINDOW *ablak;
    ablak = initscr();

    int x = 0;
    int y = 0;

    int xnov = 0;
    int ynov = 0;

    int my, mx;
    getmaxyx(ablak, my, mx);
    for(;;)
    {
        x = (x-1)%mx;
        xnov = (xnov+1)%mx;
        y = (y-1)%my;
        ynov = (ynov+1)%my;
        clear();
        mvprintw(abs((y+(my-ynov))), abs((x+(mx-xnov))),"o");
        refresh();
        usleep(100000);
    }
    return 0;
}
