#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0; // oszlop
    int y = 0;  // sor

    int xnov = 1; // karakter mennyiseg amivel halad x tengelyen (vizszintesen)
    int ynov = 1; // karakter mennyiseg amivel halad y tengelyen (fuggolegesen)

    int mx;  // oszlopok szama
    int my;  // sorok szama

    for ( ;; ) {

        getmaxyx ( ablak, my , mx ); // az aktualis ablak merete szerint megadja az mx es my integereknek az erteket
                                     // ezeket manipulalni peldaul ugy, hogy ha csak fuggolegesen feleben akarjuk mozgatni
                                     // mx integert manipulaljuk: mx = mx/2,
                                     // hasonloan my-nal (vizszintesen tudjuk manipulalni/korlatozni)
        // mx = mx/2;
        // my = my/2;
        mvprintw ( y, x, "o" );

        refresh ();
        usleep ( 100000 ); // befolyasolja a folyamatnak a sebesseget, minel kisebb szam annal gyorsabb

        clear(); // ezzel a program beirt karaktereit helyettesitjuk uresekkel

        x = x + xnov; // lepunk megadott ertekek szerint
        y = y + ynov; 

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        if ( y<=0 ) { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        if ( y>=my-1 ) { // elerte-e a aljat?
            ynov = ynov * -1;
        }

    }

    return 0;
}
