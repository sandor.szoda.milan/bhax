#include <iostream>
using namespace std;

int main()
{
	int a = 6;
	int b = 9;

	cout << "Csere elott: " << "a: " << a << " b: " << b << "\n";
	a = a + b;
	b = a - b;
	a = a - b;
	cout << "Csere utan (a): " << a << "\n";
	cout << "Csere utan (b): " << b << "\n"; 
	return 0;
}
